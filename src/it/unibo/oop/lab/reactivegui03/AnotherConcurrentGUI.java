package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnotherConcurrentGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8710276539980695794L;
	private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    
    public AnotherConcurrentGUI() {
    	super();
    	final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        new Thread(agent).start();
        
        up.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.goUp();
			}
		});
        
        down.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.goDown();
			}
		});
        
        stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.stopCounting();
				up.setEnabled(false);
				down.setEnabled(false);
				stop.setEnabled(false);
			}
		});
           
    }
    
    public void stopCounting() {
		// TODO Auto-generated method stub
		Agent.stopCounting();
    	SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				stop.setEnabled(false);
				up.setEnabled(false);
				down.setEnabled(false);
			}
		});
	}
    
    private class Agent extends Thread {
    	
    	private volatile boolean stop;
    	private volatile boolean up;
    	private int counter;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
						}
					});
					
					counter += up? 1 : -1;
					Thread.sleep(100);
				} catch (InvocationTargetException | InterruptedException ex) {
					  ex.printStackTrace();
				}
			}
		}
		
		public void stopCounting() {
			this.stop = true;
		}
		
		public void goUp() {
			this.up = true; 
		}
		
		public void goDown() {
			this.up = false;
		}
    	
    }
    
    private class SilentAgent implements Runnable {
    	
    	private static final int TIME = 10000;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AnotherConcurrentGUI.this.stopCounting();
		}
    	
    }

}
