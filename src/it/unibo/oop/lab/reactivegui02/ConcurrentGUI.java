package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class ConcurrentGUI extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    
    public ConcurrentGUI() {
    	super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	JPanel panel = new JPanel();
    	panel.add(display);
    	panel.add(up);
    	panel.add(down);
    	panel.add(stop);
    	this.getContentPane().add(panel);
    	this.setVisible(true);
    	
    	final Agent agent = new Agent();
    	new Thread(agent).start();
    	
    	up.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.goUp();
			}
		});
    	
    	down.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.goDown();
			}
		});
    	
    	stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				agent.stopCounting();
				up.setEnabled(false);
				down.setEnabled(false);
			}
		});
    	
    }
    
    private class Agent implements Runnable {
        /*
         * stop is volatile to ensure ordered access
         */
        private volatile boolean stop;
        private volatile boolean up;
        private int counter;

        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    counter += up? 1 : -1;
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }

        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }
        
        public void goUp() {
        	this.up = true;
        }
        
        public void goDown() {
        	this.up = false;
        }

    }
}
